#!/bin/sh
# Tabs are important to Xmenu
# :setlocal noexpandtab if using vim/neovim

cat <<EOF | xmenu
[]= Tiled	0
><> Floating	1
[M] Monocle	2
HHH Grid	3
TTT BStack	4
|M| Centered Master	5
[@] Fib-Dwindle	6
EOF

