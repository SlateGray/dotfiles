# region Copyright Information
# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# endregion

# Requirements:
# Hack Nerd Font Mono
# Font Awesome
# And other bound programs

# TODO: Clean up keybinds - log gets flooded by resize commands.

# region Imports
import os
import subprocess
from typing import List  # noqa: F401
from libqtile.config import Drag, Key, Screen, Group, Click
# from libqtile.config import Rule, DropDown, ScratchPad
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
import pywal_colors
# endregion

# region User variables
# mod4 or mod = super key
mod = "mod4"
mod1 = "alt"
mod2 = "control"
home = str(os.path.expanduser('~'))
myTerm = "kitty"
colors = pywal_colors.colors
# endregion

# region Key Bindings
keys = [

    # FUNCTION KEYS
    # Key([], "F12", lazy.group['scratchpad'].dropdown_toggle('term')),

    # SUPER + LETTER KEYS
    Key([mod], "q", lazy.window.kill()),
    Key([mod], "w", lazy.spawn('firefox')),
    Key([mod], "g", lazy.spawn('google-chrome-stable')),
    Key([mod], "f", lazy.spawn('thunar')),
    Key([mod], "c", lazy.spawn('vscodium')),
    Key([mod], "m", lazy.window.toggle_fullscreen()),
    # Shift+Left/Right Arrow to switch between Rofi Modi.
    # Using drun for AppImage support.
    Key([mod], "r", lazy.spawn('rofi -show drun')),
    Key([mod], "z", lazy.spawn(
        'rofi -show calc -modi calc -no-show-match -no-sort')),
    Key([mod], "x", lazy.spawn('arcolinux-logout')),
    Key([mod], "Escape", lazy.spawn('xkill')),
    Key([mod], "Return", lazy.spawn(myTerm)),
    Key([mod], "p", lazy.spawn('keepassxc')),
    # 'emacsclient -c' if using emacs daemon
    Key([mod], "d", lazy.spawn('emacs')),
    Key([mod], "v", lazy.spawn('kitty -e ranger')),

    # VARIETY KEYS WITH PYWAL
    # Next wallpaper and refresh Wal
    Key([mod, "shift"], "n", lazy.spawn(
        home + '/.config/qtile/scripts/set-pywal.sh -n')),
    # Update wal to current desktop
    Key([mod, "shift"], "u", lazy.spawn(
        home + '/.config/qtile/scripts/set-pywal.sh -u')),
    # Previous wallpaper and refresh Wal
    Key([mod, "shift"], "p", lazy.spawn(
        home + '/.config/qtile/scripts/set-pywal.sh -p')),

    # SUPER + SHIFT KEYS
    Key([mod, "shift"], "Return", lazy.spawn('st')),
    Key([mod, "shift"], "q", lazy.window.kill()),
    Key([mod, "shift"], "r", lazy.restart()),
    Key([mod, "shift"], "w", lazy.spawn('firefox --private-window')),

    # CONTROL + ALT KEYS , Mod1 = "alt"
    Key(["mod1", "control"], "m", lazy.spawn('pavucontrol')),

    # ALT + KEYS

    # CONTROL + SHIFT KEYS
    Key(["control", "shift"], "Escape", lazy.spawn('xfce4-taskmanager')),

    # SCREENSHOTS
    Key([], "Print", lazy.spawn(
        "scrot 'Z-LT-%Y-%m-%d-%s_scrot_$wx$h.jpg' -e \
               'mv $f ~/Pictures/scrots/'")),
    # Key("control", "Print", lazy.spawn('xfce4-screenshooter')),
    # Key("control", "shift", "Print", lazy.spawn('gnome-screenshot -i')),

    # MULTIMEDIA KEYS
    # INCREASE/DECREASE BRIGHTNESS
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 5")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 5")),

    # INCREASE/DECREASE/MUTE VOLUME
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -q set Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -q set Master 5%+")),

    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),
    Key([], "XF86AudioStop", lazy.spawn("playerctl stop")),

    #    Key([], "XF86AudioPlay", lazy.spawn("mpc toggle")),
    #    Key([], "XF86AudioNext", lazy.spawn("mpc next")),
    #    Key([], "XF86AudioPrev", lazy.spawn("mpc prev")),
    #    Key([], "XF86AudioStop", lazy.spawn("mpc stop")),

    # QTILE LAYOUT KEYS
    # layout.normalize resizes clients, layout.reset resizes all windows.
    # Key([mod], "n", lazy.layout.ormalize()),
    Key([mod], "n", lazy.layout.reset()),
    Key([mod], "space", lazy.next_layout()),
    Key([mod, "shift"], "space", lazy.prev_layout()),
    # Quick change back to MonadTall
    Key([mod], "F5", lazy.group.setlayout('monadtall')),

    # CHANGE FOCUS
    # For when floating windows get lost
    Key(["mod1"], "Tab", lazy.group.next_window()),
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    Key([mod], "period", lazy.next_screen()),
    Key([mod], "comma", lazy.prev_screen()),

    # RESIZE UP, DOWN, LEFT, RIGHT
    Key([mod, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([mod, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([mod, "control"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([mod, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),
    Key([mod, "control"], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),

    # FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([mod, "shift"], "f", lazy.layout.flip()),

    # FLIP LAYOUT FOR BSP
    Key([mod, "mod1"], "k", lazy.layout.flip_up()),
    Key([mod, "mod1"], "j", lazy.layout.flip_down()),
    Key([mod, "mod1"], "l", lazy.layout.flip_right()),
    Key([mod, "mod1"], "h", lazy.layout.flip_left()),

    # MOVE WINDOWS
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Left", lazy.layout.swap_left()),
    Key([mod, "shift"], "Right", lazy.layout.swap_right()),

    # TOGGLE FLOATING LAYOUT
    Key([mod, "mod1"], "space", lazy.window.toggle_floating()),
]
# endregion

# region MOUSE CONFIGURATION
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]
# endregion

# region Groups
groups = []

group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", ]

group_labels = ["", "", "", "", "", "", "", "", "", "", ]
# group_labels = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X",]

group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall",
                 "floating", "monadtall", "monadtall", "monadtall",
                 "monadtall", "monadtall",
                 ]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([

        # CHANGE WORKSPACES
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key([mod], "Tab", lazy.screen.next_group()),
        Key([mod, "shift"], "Tab", lazy.screen.prev_group()),
        Key([mod, "control"], "Tab", lazy.screen.toggle_group()),
        Key(["mod1", "shift"], '1', lazy.to_screen(1)),
        Key(["mod1", "shift"], '2', lazy.to_screen(2)),

        # MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        Key([mod, "control"], i.name, lazy.window.togroup(i.name)),
        # MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW
        Key([mod, "shift"], i.name, lazy.window.togroup(
            i.name), lazy.group[i.name].toscreen()),
    ])

# Scratchpad
# groups.append(
#     ScratchPad("scratchpad", [
#         DropDown("term", myTerm, opacity = 1, height = 0.55, width = 0.8)])
# )
# endregion

# region Layouts
layout_theme = {"margin": 2,
                "border_width": 2,
                "single_border_width": 2,
                "single_margin": 1,
                "border_focus": colors[1],  # Focused Window
                "border_normal": colors[2],  # Unfocused Window
                }

layouts = [
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Max(**layout_theme),
    layout.Floating(**layout_theme),
    # layout.Columns(
    #     num_columns=3,
    #     insert_position=1,
    #     wrap_focus_columns=True,
    #     **layout_theme),
    # layout.Stack(num_stacks=2, **layout_theme),
    # layout.Bsp(**layout_theme),
    # layout.Matrix(**layout_theme),
    layout.RatioTile(**layout_theme),
    # layout.Tile(**layout_theme),
    # layout.VerticalTile(**layout_theme),
    # layout.TreeTab(**layout_theme),
    # layout.Zoomy(**layout_theme),
]
# endregion

# region Widgets
# WIDGETS FOR THE BAR
widget_defaults = dict(font="Hack Nerd Font Mono",
                       fontsize=16,
                       padding=1,
                       background=colors[0],
                       foreground=colors[7]
                       )


def init_widgets_list():
    widgets_list = [
        widget.GroupBox(
            active=colors[2],
            block_highlight_text_color=colors[1],
            borderwidth=1,
            disable_drag=True,
            hide_unused=False,
            # highlight_color=[colors[0], colors[1]],
            highlight_method='line',  # 'border', 'block', 'text', 'line'
            inactive=colors[8],
            margin_x=0,  # margin=3 is default
            other_current_screen_border='#00FFFF',
            other_screen_border='#FF0000',
            rounded=False,
            this_current_screen_border=colors[2],
            this_screen_border='#FFFF00',
            urgent_alert_method='block',
        ),
        widget.Sep(
            linewidth=1,
            padding=10,
            foreground=colors[4],
        ),
        widget.CurrentLayoutIcon(
            scale=0.7,
        ),
        widget.Sep(
            linewidth=1,
            padding=10,
            foreground=colors[4],
        ),
        widget.WindowName(
            fontsize=10,
            foreground=colors[7],
        ),
        widget.Sep(
            linewidth=1,
            padding=10,
            foreground=colors[4],
        ),
        widget.CPUGraph(
            border_color=colors[2],
            fill_color=colors[7],
            graph_color=colors[7],
            border_width=1,
            line_width=1,
            core="all",
            type="box",  # 'box', 'line', 'linefill'
            mouse_callbacks={
                'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e htop')},
        ),
        widget.Sep(
            linewidth=1,
            padding=10,
            foreground=colors[4],
        ),
        widget.TextBox(
            font="FontAwesome",
            text="  ",
            foreground=colors[2],
            padding=0,
            fontsize=12
        ),
        widget.Memory(
            format='{MemUsed}M',  # /{MemTotal}M',
            update_interval=1,
            fontsize=10,
            foreground=colors[7],
        ),
        widget.Sep(
            linewidth=1,
            padding=10,
            foreground=colors[4],
        ),
        widget.TextBox(
            font="FontAwesome",
            text="  ",
            foreground=colors[2],
            padding=0,
            fontsize=12
        ),
        widget.Clock(
            foreground=colors[7],
            fontsize=10,
            format="%H:%M %m-%d-%Y"
        ),
        widget.Sep(
            linewidth=1,
            padding=10,
            foreground=colors[4],
        ),
        widget.Systray(
            icon_size=14,
            padding=4
        ),
    ]
    return widgets_list


widgets_list = init_widgets_list()
# endregion

# region Screens
# CLEAN THIS UP
# def init_widgets_screen1():
#     widgets_screen1 = init_widgets_list()
#     return widgets_screen1


# def init_widgets_screen2():
#     widgets_screen2 = init_widgets_list()
#     return widgets_screen2

# # def init_widgets_screen2():
# #     widgets_screen2 = [
# #             widget.CurrentLayoutIcon(
# #             scale=0.7,),
# #             widget.Sep(
# #             linewidth=1,
# #             padding=10,
# #             foreground=colors[4],),
# #             widget.WindowName(
# #             fontsize=10,
# #             foreground=colors[7],),
# #             ],
# #     return widgets_screen2

# widgets_screen1 = init_widgets_screen1()
# widgets_screen2 = init_widgets_screen2()


# def init_screens():
#     return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), size=20)),
#             Screen(top=bar.Bar(widgets=init_widgets_screen2(), size=20))
#             ]


# screens = init_screens()
screens = [
    Screen(
        top=bar.Bar(widgets=init_widgets_list(), size=20)),
    Screen(
        top=bar.Bar([
            widget.AGroupBox(
                borderwidth=0,),
            widget.Sep(
                linewidth=1,
                padding_x=5,
                foreground=colors[4],),
            widget.CurrentLayoutIcon(
                scale=0.7,),
            widget.Sep(
                linewidth=1,
                padding=10,
                foreground=colors[4],),
            widget.WindowName(
                fontsize=10,
                foreground=colors[7],),
            widget.Sep(
                linewidth=1,
                padding=10,
                foreground=colors[4],),
            widget.TextBox(
                font="FontAwesome",
                text="  ",
                foreground=colors[2],
                padding=0,
                fontsize=12),
            widget.Clock(
                foreground=colors[7],
                fontsize=10,
                format="%H:%M %m-%d-%Y",),
        ], size=20))
]
# endregion

# # region Hooks
# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
# @hook.subscribe.client_new
# def assign_app_group(client):
#     d={}
# #    d["1"] = ["Placeholder", ]
# #    d["2"] = ["Placeholder", ]
# #    d["3"] = ["Placeholder", ]
# #    d["4"] = ["gimp", ]
# #    d["5"] = ["Placeholder" ]
# #    d["6"] = ["Placeholder" ]
# #    d["7"] = ["Placeholder", ]
# #    d["8"]=["discord", ]
# #    d["9"] = ["Placeholder" ]
# #    d["0"] = ["Placeholder" ]

#     wm_class=client.window.get_wm_class()[0]

#     for i in range(len(d)):
#         if wm_class in list(d.values())[i]:
#             group=list(d.keys())[i]
#             client.togroup(group)
#             client.group.cmd_toscreen()


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])


@hook.subscribe.client_new
def floating_size_hints(window):
    hints = window.window.get_wm_normal_hints()
    if hints and 0 < hints['max_width'] < 1000:
        window.floating = True

# @hook.subscribe.startup # Disabled but left in config for possible future use
# def start_always():
#     # Set the cursor to something sane in X
#     subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])
# endregion


dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = False
bring_front_click = True
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
    {'wmclass': 'Arcolinux-welcome-app.py'},
    {'wmclass': 'Arcolinux-tweak-tool.py'},
    {'wname': 'Open File'},
    {'wname': 'Search'},
])
auto_fullscreen = True
focus_on_window_activation = "smart"

wmname = "LG3D"  # Java Whitelist WM
