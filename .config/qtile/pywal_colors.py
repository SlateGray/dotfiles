import json
from pathlib import Path

cache = Path(Path.home(), '.cache/wal/colors.json')
nopywal = Path('nopywal.json')

if cache.exists():
    with open(cache, 'r') as colors_file:
        colors_dict = json.load(colors_file)
        # print("Found it")
        # print(cache)
else:
    with open(nopywal, 'r') as colors_file:
        colors_dict = json.load(colors_file)
        # print("Didn't find it")

colors = list(colors_dict['colors'].values())
